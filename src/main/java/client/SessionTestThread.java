/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package client;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

class SessionTestThread implements Runnable, WorkerThread {

    private final static Logger log = LoggerFactory.getLogger(SessionTestThread.class);

    private final java.util.Properties config = new java.util.Properties();

    private final String host;
    private final int port;
    private final String user;
    private final String pass;
    private final String remote;

    private volatile int returnValue;

    private Duration duration;
    private Instant startTime;

    private JSch jsch = new JSch();
    private Session session = null;
    private ChannelSftp channel;

    public SessionTestThread(String host, int port, String user, String pass, String remote) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.pass = pass;
        this.remote = remote;
        config.put("StrictHostKeyChecking", "no");
    }

    @Override
    public void run() {

        try {
            log.info("run() - connecting");
            startTime = Instant.now();

            session = jsch.getSession(user, host, port);
            session.setPassword(pass);
            session.setConfig(config);
            session.connect();

            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            channel.cd(remote);

            loop();

            channel.exit();
            returnValue = 0;

        } catch (JSchException e) {
            log.error("run() - JSch error: " + e.getMessage(), e);
            returnValue = 1;
        } catch (SftpException e) {
            log.error("run() - sftp error: " + e.getMessage(), e);
            returnValue = 2;
        } catch (InterruptedException e) {
            log.error("run() - thread error: " + e.getMessage(), e);
        } finally {
            if (session != null) {
                log.info("run() - disconnecting");
                session.disconnect();
            }

        }

    }

    public int getReturnValue() {
        return returnValue;
    }

    private void loop() throws SftpException, InterruptedException {

        int i = 0;
        do {

            i+=10;
            duration = Duration.between(startTime, Instant.now());

            String str = String.format("Session test - sleep: %d, duration: %s", i, duration.toString());
            byte[] bytes = str.getBytes();

            channel.put(new ByteArrayInputStream(bytes),"test.txt");
            log.info(str);
            Thread.sleep(i * 1000);
        } while (true);
    }

}
