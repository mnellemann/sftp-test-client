/*
   Copyright 2021 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

@Command(name = "sftp-test-client",
    mixinStandardHelpOptions = true, sortOptions = false,
    versionProvider = VersionProvider.class)
public class Application implements Callable<Integer> {

    private final static Logger log = LoggerFactory.getLogger(Application.class);


    @CommandLine.Option(names = {"-H", "--host"}, description = "Destination host (default: ${DEFAULT-VALUE}).", paramLabel = "host", required = true)
    static String destinationHost;

    @CommandLine.Option(names = {"-P", "--port"}, description = "Destination port (default: ${DEFAULT-VALUE}).", paramLabel = "port")
    private int destinationPort = 22;

    @CommandLine.Option(names = {"-u", "--username"}, description = "Destination username (default: ${DEFAULT-VALUE}).", paramLabel = "user", required = true)
    static String username;

    @CommandLine.Option(names = {"-p", "--password"}, description = "Destination password (default: ${DEFAULT-VALUE}).", paramLabel = "pass", required = true)
    static String password;

    @CommandLine.Option(names = {"-r", "--repeat"}, description = "Repeat this many times (default: ${DEFAULT-VALUE}).", paramLabel = "num")
    private int repeats = 1;

    @CommandLine.Option(names = {"-w", "--wait"}, description = "Time to wait between repeats (default: ${DEFAULT-VALUE}).", paramLabel = "sec")
    private int waitTime = 60;

    @CommandLine.Option(names = {"-s", "--source"}, description = "Local path to upload files from.", paramLabel = "path", required = true)
    static File source;

    @CommandLine.Option(names = {"-d", "--destination"}, description = "Remote folder to upload files into (must exist).", paramLabel = "path", required = true)
    static String remotePath;

    @CommandLine.Option(names = {"-t", "--test"}, description = "Long running session test, uploads 1 status file continuously, and increases sleep time in between.")
    static boolean test;

    public static void main(String... args) {
        int exitCode = new CommandLine(new Application()).execute(args);
        System.exit(exitCode);
    }


    @Override
    public Integer call() throws Exception {

        Duration duration;
        Instant startTime;
        Instant endTime;

        AtomicBoolean keepRunning = new AtomicBoolean(true);
        Thread shutdownHook = new Thread(() -> {
            keepRunning.set(false);
            System.out.println("Stopping sftp-test-client, please wait ...");
        });
        Runtime.getRuntime().addShutdownHook(shutdownHook);


        int runs = 1;
        do {
            int returnValue = 0;
            startTime = Instant.now();

            try {
                WorkerThread sftpThread;
                if(test) {
                    sftpThread = new SessionTestThread(destinationHost, destinationPort, username, password, remotePath);

                } else {
                    sftpThread = new UploadThread(destinationHost, destinationPort, username, password, source, remotePath);
                }

                Thread thread = new Thread(sftpThread);
                //thread.setName(source.getName());
                thread.start();
                thread.join();
                returnValue = sftpThread.getReturnValue();
            } catch(Exception e) {
                log.error(e.getMessage());
            }

            endTime = Instant.now();
            duration = Duration.between(startTime, endTime);
            log.info(String.format("Duration: %s\tStatus: %d", duration.toString(), returnValue));

            if(++runs > repeats) {
                keepRunning.set(false);
            } else {
                Thread.sleep(waitTime * 1000);
            }

        } while (keepRunning.get());

        return 0;
    }

}
