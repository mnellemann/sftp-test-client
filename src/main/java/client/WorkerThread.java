package client;

public interface WorkerThread extends Runnable {
    public int getReturnValue();
}
