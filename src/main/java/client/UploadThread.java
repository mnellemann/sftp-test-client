/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package client;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Objects;

class UploadThread implements Runnable, WorkerThread {

    private final static Logger log = LoggerFactory.getLogger(UploadThread.class);

    private final java.util.Properties config = new java.util.Properties();

    private final String host;
    private final int port;
    private final String user;
    private final String pass;
    private final String remote;
    private final File source;

    private volatile int returnValue;

    private JSch jsch = new JSch();
    private Session session = null;
    private ChannelSftp channel;

    public UploadThread(String host, int port, String user, String pass, File source, String remote) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.pass = pass;
        this.remote = remote;
        this.source = source;
        config.put("StrictHostKeyChecking", "no");
    }

    @Override
    public void run() {

        try {
            session = jsch.getSession(user, host, port);
            session.setPassword(pass);
            session.setConfig(config);
            session.connect();

            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            channel.cd(remote);

            explore(source);

            channel.exit();
            returnValue = 0;

        } catch (JSchException e) {
            log.error("run() - JSch error: "+  e.getMessage(), e);
            returnValue = 1;
        } catch (SftpException e) {
            log.error("run() - sftp error: "+  e.getMessage(), e);
            returnValue = 2;
        } finally {
            if (session != null) {
                session.disconnect();
            }
        }

    }

    public int getReturnValue() {
        return returnValue;
    }



    protected void explore(File dir) {

        log.debug("explore() - " + dir.toString());

        if(!dir.exists() || !dir.isDirectory()) {
            log.debug("explore() - skipping, non-existing directory.");
            return;
        }

        File[] files = dir.listFiles();
        for(File f : Objects.requireNonNull(files)) {

            if(f.isDirectory()) {
                explore(f);
            } else {
                log.info("explore() - processing: " + f);
                try {
                    process(f);
                } catch (FileNotFoundException e) {
                    log.error("explore() - file not found: " + e.getMessage());
                } catch (SftpException e) {
                    log.error("explore() - sftp error: " + e.getMessage());
                }
            }
        }

    }

    private void process(File file) throws FileNotFoundException, SftpException {
        log.debug("Uploading file: " + file.getName());
        channel.put(new FileInputStream(file), file.getName());
    }

}
