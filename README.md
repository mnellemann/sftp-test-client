# sftp-test-client

Upload local files to a remote sftp server.

## Requirements

You need Java (JRE) version 8 or later to run jload.

## Usage Instructions

- Install the sftp-test-client package (*.deb*, *.rpm* or *.jar*) from [downloads](https://bitbucket.org/mnellemann/sftp-test-client/downloads/) or compile from source.
- Run **/opt/sftp-test-client/bin/sftp-test-client**, if installed from package
- Or as **java -jar /path/to/sftp-test-client.jar**


```text
Usage: sftp-test-client [-htV] -d=path -H=host -p=pass [-P=port] [-r=num]
                        -s=path -u=user [-w=sec]
  -H, --host=host          Destination host (default: null).
  -P, --port=port          Destination port (default: 22).
  -u, --username=user      Destination username (default: null).
  -p, --password=pass      Destination password (default: null).
  -r, --repeat=num         Repeat this many times (default: 1).
  -w, --wait=sec           Time to wait between repeats (default: 60).
  -s, --source=path        Local path to upload files from.
  -d, --destination=path   Remote folder to upload files into (must exist).
  -t, --test               Long running session test, uploads 1 status file
                             continuously, and increases sleep time in between.
  -h, --help               Show this help message and exit.
  -V, --version            Print version information and exit.
```

## Development Information

You need Java (JDK) version 8 or later to build jload.

### Build & Test

Use the gradle build tool, which will download all required dependencies:

```shell
./gradlew clean build
```
